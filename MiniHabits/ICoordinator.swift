//
//  ICoordinator.swift
//  MiniHabits
//
//  Created by Халимка on 23.06.2023.
//

import Foundation

import UIKit

protocol ICoordinatorFinishDelegate {
    func didFinish(_ coordinator: ICoordinator)
}

protocol ICoordinator: AnyObject {
    var navigationController: UINavigationController {get set}
    var finishDelegate: ICoordinatorFinishDelegate? {get set}
    var childCoordinators: [ICoordinator] {get set}
    func start()
    func finish()
}

extension ICoordinator {
    func finish(){
        childCoordinators.removeAll()
        finishDelegate?.didFinish(self)
    }
}
