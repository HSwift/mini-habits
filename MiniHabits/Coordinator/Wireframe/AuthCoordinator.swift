//
//  AuthCoordinator.swift
//  MiniHabits
//
//  Created by Халимка on 24.05.2023.
//

import Foundation
import UIKit

protocol IAuthCoordinator: ICoordinator {
    func showAuthScreen()
}

class AuthCoordinator: IAuthCoordinator {
    var navigationController: UINavigationController
    var finishDelegate: ICoordinatorFinishDelegate?
    var childCoordinators: [ICoordinator] = []
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func showAuthScreen() {
        let viewController = AuthScreenAssembly.assembly()
        navigationController.pushViewController(viewController, animated: true)
    }
    
  
    
    func start() {
        
    }
    
    
}
