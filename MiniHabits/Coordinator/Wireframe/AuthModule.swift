//
//  AuthModule.swift
//  MiniHabits
//
//  Created by Халимка on 09.06.2023.
//


import UIKit

class AuthModule {
    
    static func build() -> UIViewController {
        let view = AuthViewController()
        let authService = AuthService()
        let interactor = AuthInteractor(authService: authService)
//        let router = AuthRouter()
        let presenter = AuthPresenter()
        
        view.presenter = presenter
        interactor.presenter = presenter
//        router.view = view
        
        return view
    }
}
