//
//  AuthScreenAssembly.swift
//  MiniHabits
//
//  Created by Халимка on 23.06.2023.
//

import UIKit

final class AuthScreenAssembly: Assembly {
    
    static func assembly() -> UIViewController {
        
        let view = AuthViewController()
        let authService = AuthService()
        let interactor = AuthInteractor(authService: authService)
        let coordinator = AuthCoordinator()
        let presenter = AuthPresenter()
        
        view.presenter = presenter
        interactor.presenter = presenter
        
        return view
    }
}
