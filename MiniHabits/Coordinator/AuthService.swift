//
//  AuthService.swift
//  MiniHabits
//
//  Created by Халимка on 08.06.2023.
//

import Foundation

enum AuthError: Error {
    case invalidCredentials
}

protocol AuthServiceOutput {
    func login(username: String, password: String, completion: @escaping (Result<Void, AuthError>) -> Void)
    func saveCredentials(username: String, password: String)
}

class AuthService {}
    
extension AuthService: AuthServiceOutput {
    func login(username: String, password: String, completion: @escaping (Result<Void, AuthError>) -> Void) {
        let savedUsername = UserDefaults.standard.string(forKey: "username")
        let savedPassword = UserDefaults.standard.string(forKey: "password")
        
        if username == savedUsername && password == savedPassword {
            completion(.success(()))
        } else {
            completion(.failure(.invalidCredentials))
        }
    }
    
    func saveCredentials(username: String, password: String) {
        UserDefaults.standard.set(username, forKey: "username")
        UserDefaults.standard.set(password, forKey: "password")
    }
}


