//
//  AuthInteractor.swift
//  MiniHabits
//
//  Created by Халимка on 24.05.2023.
//

import Foundation


final class AuthInteractor {
    weak var presenter: AuthInteractorOutput?
    var authService: AuthServiceOutput
    
    init(authService: AuthServiceOutput){
        self.authService = authService
    }
}

// MARK: - AuthInteractorInput
extension AuthInteractor: AuthInteractorInput {

    func performLogin(username: String, password: String, completion: @escaping (Result<Void, Error>) -> Void) {
        
        
        authService.login(username: "A", password: "P") { result in
            switch result {
            case .success:
                self.authService.saveCredentials(username: "A", password: "A")
                print("Аутентификация успешна")
            case .failure(let error):
                print("Ошибка аутентификации: \(error)")
            }
        }

    }
}


