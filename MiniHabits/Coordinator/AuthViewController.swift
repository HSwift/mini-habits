//
//  AuthViewController.swift
//  MiniHabits
//
//  Created by Халимка on 24.05.2023.
//

import UIKit
import PinLayout

class AuthViewController: UIViewController {
    
    // MARK: - Properties
    
    var presenter: AuthViewOutput?
    private var email: String?
    
    
    // MARK: - Views
    
    
    private lazy var logoImageView: UIImageView = {
        let logoImageView = UIImageView()
        logoImageView.image = UIImage(systemName: "person.circle")
        return logoImageView
    }()
    private lazy var usernameTextField: UITextField = {
        let usernameTextField = UITextField()
        usernameTextField.placeholder = "Имя пользователя"
        return usernameTextField
    }()
    private lazy var passwordTextField: UITextField = {
        let passwordTextField = UITextField()
        passwordTextField.placeholder = "Пароль"
        return passwordTextField
    }()
    private lazy var loginButton: UIButton = {
        let loginButton = UIButton()
        loginButton.setTitle("Войти", for: .normal)
        loginButton.backgroundColor = .blue
        loginButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return loginButton
    }()
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let loadingIndicator = UIActivityIndicatorView()
        loadingIndicator.style = .medium
        return loadingIndicator
    }()
  
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Регистрация"
        label.textAlignment = .center
        return label
    }()
    
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.insertSublayer(gradientView(), at: 0)
        setNavigationBar()
        setupSubviews()
    }
    
    
    // MARK: - Draw
    
    private func setNavigationBar() {
        let logoImageView = UIImageView(image: UIImage(named: "header"))
        logoImageView.frame = CGRect(x: -40, y: 0, width: 150, height: 40)
        logoImageView.contentMode = .scaleAspectFit
        let imageItem = UIBarButtonItem(customView: logoImageView)
        navigationItem.leftBarButtonItems = [imageItem]
    }
    
    private func setupSubviews() {
        
        view.backgroundColor = UIColor.white
        
        view.addSubview(titleLabel)
        titleLabel.pin
            .top(100)
            .hCenter()
            .width(200)
            .height(50)
        
        view.addSubview(logoImageView)
        logoImageView.pin
            .below(of: titleLabel, aligned: .center)
            .size(CGSize(width: 100, height: 100))
        
        view.addSubview(usernameTextField)
        usernameTextField.pin
            .below(of: logoImageView, aligned: .center)
            .width(200)
            .height(100)

        view.addSubview(passwordTextField)
        passwordTextField.pin
            .below(of: usernameTextField, aligned: .center)
            .width(200)
            .height(100)
        
        view.addSubview(loginButton)
        loginButton.pin
            .below(of: passwordTextField, aligned: .center)
            .width(200)
            .height(50)
        
        view.addSubview(loadingIndicator)
            loadingIndicator.pin
            .hCenter()
            .vCenter()
        }
    
    
    // MARK: - Actions
    
    @objc private func buttonTapped() {
        guard let username = usernameTextField.text, let password = passwordTextField.text else {
            return
        }
            presenter?.loginButtonTapped(username: username, password: password)
    }
    
    func gradientView() -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
         gradientLayer.colors = [UIColor.white.cgColor, UIColor.systemPink.cgColor]
         gradientLayer.startPoint = CGPoint(x: 0, y: 0)
         gradientLayer.endPoint = CGPoint(x: 1, y: 1)
         gradientLayer.frame = view.bounds
         return gradientLayer
    }
}
    

// MARK: - AuthViewInput
extension AuthViewController: AuthViewInput {
    func showErrorMessage(_ message: String) {
        
    }
    
    func showLoadingIndicator() {
        loadingIndicator.startAnimating()
    }
    
    func hideLoadingIndicator() {
        loadingIndicator.stopAnimating()
    }
}

//MARK: UITextFieldDelegate
extension AuthViewController: UITextFieldDelegate {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
}
