//
//  AuthPresenter.swift
//  MiniHabits
//
//  Created by Халимка on 24.05.2023.
//

import Foundation

final class AuthPresenter {
    weak var view: AuthViewInput?
    var interactor: AuthInteractorInput?
    var router =  MainScreenRouter()
}


// MARK: - AuthViewOutput
extension AuthPresenter: AuthViewOutput {
    
    func loginButtonTapped(username: String, password: String) {
         view?.showLoadingIndicator()
        
        interactor?.performLogin(username: username, password: password) { [weak self] result in
            self?.view?.hideLoadingIndicator()
            switch result {
            case .success:
                self?.router.openHomeScreen()
                
            case .failure(let error):
                self?.view?.showErrorMessage(error.localizedDescription)
                self?.router.openHomeScreen()
            }
        }
        view?.showLoadingIndicator()
    }
}

// MARK: - AuthViewOutput
extension AuthPresenter: AuthInteractorOutput {
    
}

