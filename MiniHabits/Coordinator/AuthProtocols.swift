//
//  AuthProtocols.swift
//  MiniHabits
//
//  Created by Халимка on 24.05.2023.
//

import Foundation

protocol AuthRouterInput: AnyObject {

}

protocol AuthInteractorInput: AnyObject {
    func performLogin(username: String, password: String, completion: @escaping (Result<Void, Error>) -> Void)
}

protocol AuthInteractorOutput: AnyObject {
 
}

protocol AuthViewInput: AnyObject {
    func showErrorMessage(_ message: String)
    func showLoadingIndicator()
    func hideLoadingIndicator()
}

protocol AuthViewOutput: AnyObject {
    func loginButtonTapped(username: String, password: String)

}

protocol AuthPresenterOutput {
    
}
