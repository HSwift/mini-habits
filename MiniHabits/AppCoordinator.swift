//
//  AppCoordinator.swift
//  MiniHabits
//
//  Created by Халимка on 23.06.2023.
//

import UIKit


protocol IAppCoordinator: ICoordinator {
  
    func showAuthFlow()
    func showMainScreenFlow()
}

final class AppCoordinator: IAppCoordinator  {
    var navigationController: UINavigationController
    var finishDelegate: ICoordinatorFinishDelegate?
    var childCoordinators: [ICoordinator] = []
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        showAuthFlow()
    }

    func showAuthFlow() {
        let authCoordinator = AuthCoordinator(navigationController: navigationController)
        childCoordinators.append(authCoordinator)
        authCoordinator.start()
    }
    
    func showMainScreenFlow() {
        
    }

}
