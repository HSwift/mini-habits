//
//  MainScreenPresenter.swift
//  MiniHabits
//
//  Created by Халимка on 07.06.2023.
//

import Foundation

final class MainScreenPresenter {
    
    // MARK: - Dependency
    
    weak var view: MainScreenViewInput?
    private let interactor: MainScreenInteractorInput?
    private let router: MainScreenRouterInput?
    
    init(view: MainScreenViewInput, interactor: MainScreenInteractorInput, router: MainScreenRouterInput) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

// MARK: - MainScreenViewOutput
extension MainScreenPresenter: MainScreenViewOutput {
    
}

// MARK: - MainScreenInteractorOutput
extension MainScreenPresenter: MainScreenInteractorOutput {
    
}
