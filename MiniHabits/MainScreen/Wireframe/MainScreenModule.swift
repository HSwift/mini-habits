//
//  MainScreenModule.swift
//  MiniHabits
//
//  Created by Халимка on 09.06.2023.
//

import UIKit

class MainScreenModule {
    
    static func build() -> UIViewController {
        let view = MainScreenViewController()
        let interactor = MainScreenInteractor()
        let router = MainScreenRouter()
        let presenter = MainScreenPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        interactor.presenter = presenter
        router.view = view
        
        return view
    }
}

