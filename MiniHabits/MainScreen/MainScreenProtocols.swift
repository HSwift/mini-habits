//
//  MainScreenProtocols.swift
//  MiniHabits
//
//  Created by Халимка on 09.06.2023.
//

import Foundation


protocol MainScreenViewInput: AnyObject  {
    
}

protocol MainScreenViewOutput: AnyObject  {
    
}

protocol MainScreenPresenterInput: AnyObject  {
    
}

protocol MainScreenPresenterOutput: AnyObject  {
    
}


protocol MainScreenInteractorInput: AnyObject  {
    
}

protocol MainScreenInteractorOutput: AnyObject  {
    
}


protocol MainScreenRouterInput: AnyObject {
    func openHomeScreen()
}
