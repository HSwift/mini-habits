//
//  MainScreenInteractor.swift
//  MiniHabits
//
//  Created by Халимка on 07.06.2023.
//

import Foundation

final class MainScreenInteractor {
    
    // MARK: - Properties
    
    weak var presenter: MainScreenInteractorOutput?
    
    
    // MARK: - Init
    
    init() {
    }
}


// MARK: - MainScreenInteractorInput
extension MainScreenInteractor: MainScreenInteractorInput {
    
}
