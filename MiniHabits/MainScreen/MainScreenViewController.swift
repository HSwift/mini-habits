//
//  MainScreenViewController.swift
//  MiniHabits
//
//  Created by Халимка on 07.06.2023.
//

import UIKit

final class MainScreenViewController: UIViewController {
    
    // MARK: - Properties
    
    var presenter: MainScreenViewOutput? = nil
    
    
    //MARK: - Views
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Мини привычки"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        return label
    }()
    
    private lazy var addButton: UIButton = {
        let button = UIButton()
        button.setTitle("Добавить привычку", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .blue
        button.addTarget(MainScreenViewController.self, action: #selector(addButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
//        tableView.register(cellTypes: )
        return tableView
    }()
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - Drawing
    private func setupUI() {
        view.backgroundColor = .white
        
        view.addSubview(titleLabel)
        titleLabel.pin
            .top(view.safeAreaInsets.top + 20)
            .left(20)
            .right(20)
            .marginTop(20)
        
        view.addSubview(addButton)
        addButton.pin
            .below(of: titleLabel)
            .left(20)
            .right(20)
            .height(50)
            .marginTop(20)
        
        view.addSubview(tableView)
        tableView.pin
            .below(of: addButton)
            .left()
            .right()
            .bottom()

    }
    
    //MARK: - Actions
    @objc private func addButtonTapped() {
        // Действие при нажатии кнопки "Добавить привычку"
    }
}


// MARK: -  UITableViewDataSource
extension MainScreenViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "", for: indexPath)
        return cell
    }
}


// MARK: - UITableViewDelegate
extension MainScreenViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 49
    }
}


// MARK: - MainScreenViewInput
extension MainScreenViewController: MainScreenViewInput {
    
}
