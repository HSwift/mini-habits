//
//  MainCoordinator.swift
//  MiniHabits
//
//  Created by Халимка on 23.06.2023.
//

import Foundation

protocol IMainCoordinator: ICoordinator {
    func showSignInFlow()
    func showSignUpFlow()
}
