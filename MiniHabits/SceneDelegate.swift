//
//  SceneDelegate.swift
//  MiniHabits
//
//  Created by Халимка on 24.05.2023.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        self.window = UIWindow(windowScene: windowScene)
        var view = AuthScreenAssembly.assembly()
        let navigationController = UINavigationController(rootViewController: view)
        let appCoordinator = AppCoordinator(navigationController: navigationController)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        appCoordinator.start()
//        self.window = window
    }


}

